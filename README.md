# Scrapy and TOR

## Install and configure TOR
```
https://jarroba.com/anonymous-scraping-by-tor-network/
```

## Install polipo (proxy caching)
```
$ sudo apt install polipo -y
```

## Configure tor port in polipo
```
$ nano /etc/polipo/config
    --- Add these three lines below (check your tor port and replace if necessary)
    socksParentProxy = localhost:9051
    diskCacheRoot=""
    disableLocalInterface=""

```

## Configure middleware
```
class RandomUserAgentMiddleware(object):
    def process_request(self, request, spider):
        ua  = random.choice(settings.USER_AGENT_LIST)
        if ua:
            request.headers.setdefault('User-Agent', ua)


class ProxyMiddleware(object):
    def process_request(self, request, spider):
        request.meta['proxy'] = settings.HTTP_PROXY
```

## Configre settings
```
USER_AGENT_LIST = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0) Gecko/16.0 Firefox/16.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10'
]

HTTP_PROXY = 'http://127.0.0.1:8123'

DOWNLOADER_MIDDLEWARES = {
     'hubstaff.middlewares.RandomUserAgentMiddleware': 400,
     'hubstaff.middlewares.ProxyMiddleware': 410,
     'hubstaff.downloadermiddlewares.useragent.UserAgentMiddleware': None
    # Disable compression middleware, so the actual HTML pages are cached
}
```

## Done, hurray! Start scraping :D :D :) ;)