
import scrapy

from hubstaff.items import HubstaffItem


class TescoSpider(scrapy.Spider):
    name = "tesco"

    allowed_domains = ['https://www.tesco.com/']
    start_urls = ['https://www.tesco.com/groceries/en-GB/products/250774576']
    # start_urls = ['https://www.tesco.com/groceries/en-GB/products/268104801']

    def parse(self, response):
        item = HubstaffItem()

        item['price'] = response.xpath("//div[@class='price-per-quantity-weight']/span/span[@class='value']/text()").extract()[0]
        print(item)
